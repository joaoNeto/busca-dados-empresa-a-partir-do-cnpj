const express = require('express');
const cheerio = require('cheerio');
const axios = require('axios');
var cors = require('cors');
const app = express();

var http = axios.create({
  baseURL: '',
  timeout: 1000,
});

let arrSitesPermitidos = [
    {nomeSite: 'consultascnpj', chamarFuncao: 'retornarDadosConsultasCnpj'}, 
    {nomeSite: 'cnpj.services', chamarFuncao: 'retornarDadosCnpjServices'}, 
    {nomeSite: 'cnpj.rocks', chamarFuncao: 'retornarDadosCnpjRocks'}
];
let url = '';

app.use(cors());
app.get('/', async (req, res) => {

    if(typeof req.query.cnpj == 'undefined'){
        res.send({
            status: false, 
            msgDetails: 'Voce precisa informar o cnpj que deseja pesquisar os dados!',
            code: 008
        });
    }

    // calculo do cnpj, pra verificar se é valido
    if(!validarCNPJ(req.query.cnpj)){
        res.send({
            status: false, 
            msgDetails: 'O cnpj informado é inválido!',
            code: 009
        });
    }

    url = 'https://www.google.com.br/search?source=hp&q=cnpj+'+req.query.cnpj;
    res.send(await main());
});
app.listen(3001);


async function main(){
    let resultado = {};
    const response = await http.get(url);
    if(response.status == 200){

        let arrLinksGoogleValidos  = [];
        let $ = cheerio.load(response.data);
        let arrLinksGoogle = [];
        // pegando a lista de links do resultado do google e pegando apenas os links que nós podemos tratar  
        $('a').each(function( index ) {
            var linkPageGoogle = $(this).attr('href');
            arrLinksGoogle.push(linkPageGoogle);
        });

        // pegando as urls validas
        arrSitesPermitidos.forEach((sitePermitido) => {
            var urlValida = arrLinksGoogle.find((link) => {
                return link.indexOf(sitePermitido.nomeSite) > 0
            });
            urlValida = urlValida.replace('/url?q=', '');
            urlValida = urlValida.substring(0, urlValida.indexOf("&"));

            arrLinksGoogleValidos.push({
                'link': urlValida,
                'chamarFuncao': sitePermitido.chamarFuncao
            });    

        });

        arrLinksGoogleValidos = randomArray(arrLinksGoogleValidos);
        for(var i = 0 ; i < arrLinksGoogleValidos.length ; i++){
            try{
                const responseSite = await http.get(arrLinksGoogleValidos[i].link);
                eval('result = '+arrLinksGoogleValidos[i].chamarFuncao+'(responseSite.data)');
                if(responseSite.status == 200 && result.status){
                    console.log(result);
                    return result; // retorna o json caso tenha encontrado os dados
                }
            }catch(err){}
        }

        // devolver mensagem padrao de cnpj nao encontrado caso nao tenha encontrado nenhum resultado dos sites
        resultado = {
            status: false, 
            msgDetails: 'não foi possivel encontrar os dados do seu cnpj',
            code: 007
        };

    } else {
        resultado = {
            status: false, 
            msgDetails: 'Erro ao acessar dados do google',
            code: 006
        };
    }
    return resultado;
}


/* ---- FUNCÕES PARA TRATAR O HTML DE CADA SITE
   ----------------------------------------- */

// http://www.consultascnpj.com/unity-brasil-tecnologia-da-informacao-ltda-me/06013344000111
function retornarDadosConsultasCnpj(stringHtml){
    let resultado = {};
    let arrElements = [];
    let $ = cheerio.load(stringHtml);

    $("strong").each(function(){
        arrElements.push($(this).html());
    });

    if(arrElements[0] != '' && typeof arrElements[0] != 'undefined'){
        resultado = {
            status: true,
            dadosCnpj: {
                cnpj: arrElements[0].replace(".", "").replace(".", "").replace("/", "").replace("-", ""),
                razao_social: arrElements[1],
                nome_fantasia: arrElements[2],
                logradouro: arrElements[15],
                numero: arrElements[16],
                cep: arrElements[17].replace(".", "").replace("-", ""),
                complemento: arrElements[18],
                bairro: arrElements[19],
                municipio: arrElements[20],
                uf: arrElements[21],
                telefone: arrElements[22].replace("(", "").replace(")", "").replace(" ", "").replace("-", "")
            }
        };
    } else {
        resultado = {status: false};
    }

    return resultado;
}

// https://cnpj.services/06013344000111/unity-brasil-tecnologia-da-informacao-ltda-me
function retornarDadosCnpjServices(stringHtml){
    let resultado = {};
    let arrElements = [];
    // let $ = cheerio.load(stringHtml);

    $("li").each(function(){
        arrElements.push($(this).html());
    });

    var cnpj = $("h2").html().replace("CNPJ ", "");

    if(cnpj != '' && typeof cnpj != 'undefined'){
        resultado = {
            status: true,
            dadosCnpj: {
                cnpj: cnpj.replace(".", "").replace(".", "").replace("/", "").replace("-", ""),
                razao_social: $("h1").html().split(" - ")[0],
                nome_fantasia: $("h1").html().split(" - ")[1],
                logradouro: arrElements[2].replace("Logradouro:", "").replace(/(?:\r\n|\r|\n)/g, ''), 
                numero: arrElements[3].replace("Numero:", "").replace(/(?:\r\n|\r|\n)/g, ''), 
                cep: arrElements[4].replace("CEP:", "").replace(/(?:\r\n|\r|\n)/g, '').replace(".", "").replace("-", ""), 
                complemento: arrElements[5].replace("Complemento:", "").replace(/(?:\r\n|\r|\n)/g, ''), 
                bairro: arrElements[6].replace("Bairro:", "").replace(/(?:\r\n|\r|\n)/g, ''), 
                municipio: arrElements[7].replace("Município:", "").replace(/(?:\r\n|\r|\n)/g, '').replace("Munic&#xED;pio:", ""),
                uf: arrElements[8].replace("UF:", "").replace(/(?:\r\n|\r|\n)/g, ''), 
                telefone: arrElements[9].replace("Telefone:", "").replace(/(?:\r\n|\r|\n)/g, '').replace("(", "").replace(")", "").replace(" ", "").replace("-", "") 
            }
        };
    } else {
        resultado = {status: false};
    }

    return resultado;
}

// https://cnpj.rocks/cnpj/06013344000111/unity-brasil-tecnologia-da-informacao-ltda-me.html
function retornarDadosCnpjRocks(stringHtml){
    let resultado = {};
    let arrElements = [];
    let $ = cheerio.load(stringHtml);

    $("strong").each(function(line){
        arrElements.push($(this).html());
    });

    if(arrElements[3] != '' && typeof arrElements[3] != 'undefined'){
        resultado = {
            status: true,
            dadosCnpj: {
                cnpj: arrElements[3].replace(".", "").replace(".", "").replace("/", "").replace("-", ""),
                razao_social: arrElements[4],
                nome_fantasia: arrElements[5],
                logradouro: arrElements[17],
                numero: arrElements[18],
                cep: arrElements[16].replace(".", "").replace("-", ""),
                complemento: arrElements[19],
                bairro: arrElements[20],
                municipio: arrElements[21],
                uf: arrElements[22],
                telefone: ''
            }
        };
    } else {
        resultado = {status: false};
    }

    return resultado;
}


/* ---- UTILS
   ----------------------------------------- */
function randomArray(arra1) {
    var ctr = arra1.length, temp, index;
    while (ctr > 0) {
        index = Math.floor(Math.random() * ctr);
        ctr--;
        temp = arra1[ctr];
        arra1[ctr] = arra1[index];
        arra1[index] = temp;
    }
    return arra1;
}

function validarCNPJ(cnpj) {
 
    cnpj = cnpj.replace(/[^\d]+/g,'');
 
    if(cnpj == '') return false;
     
    if (cnpj.length != 14)
        return false;
 
    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" || 
        cnpj == "11111111111111" || 
        cnpj == "22222222222222" || 
        cnpj == "33333333333333" || 
        cnpj == "44444444444444" || 
        cnpj == "55555555555555" || 
        cnpj == "66666666666666" || 
        cnpj == "77777777777777" || 
        cnpj == "88888888888888" || 
        cnpj == "99999999999999")
        return false;
         
    // Valida DVs
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;
         
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
          return false;
           
    return true;
    
}

